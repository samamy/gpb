# Oracle Client  

Oracle Database 19c Client (19.3) for Linux x86-64 - **LINUX.X64_193000_client.zip**   
https://www.oracle.com/database/technologies/oracle19c-linux-downloads.html

## Requirmments
`yum install cpp glibc-headers kernel-headers sysstat libX11-common libXext lm_sensors-libs libxcb libX11 libXau libXi libXtst libmpc mpfr zlib-devel gcc gcc-c++ gcc-c++-devel glibc-devel libgcc libgcc-devel libaio libaio-devel compat-libstdc++-33 libstdc++ libstdc++-devel elfutils-libelf-devel ksh compat-libcap1`

> SWAP >150MB

> RedHat 7  
>     $ yum-config-manager --enable rhel-7-server-optional-rpms  
>     $ yum install compat-libstdc++-33  


```
compat-libstdc++-33 
https://centos.pkgs.org/7/centos-x86_64/compat-libstdc++-33-3.2.3-72.el7.x86_64.rpm.html  
```

```
cat /etc/oraInst.loc
inventory_loc=/u01/app/oraInventory
inst_group=oinstall
```

```
cat /home/oracle/.bash_profile
# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs
ORACLE_HOME=/u01/app/oracle/product/19.0.0/client_1
ORACLE_BASE=/u01/app/oracle/
TNS_ADMIN=/u01/app/oracle/product/19.0.0/client_1/network/admin
PATH=$PATH:$HOME/bin:$ORACLE_HOME/bin:$HOME/.local/bin
export NLS_LANG=AMERICAN_AMERICA.UTF8
export ORACLE_HOME
export ORACLE_BASE
export TNS_ADMIN
export PATH
```
```
source ~/.bash_profile
```

` ./runInstaller -silent `


