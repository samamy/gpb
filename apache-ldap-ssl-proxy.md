> Внимание если у заказчика стоит RedHat то правильно будет подключить дополнительный репозитории из подписки rhel командой `subscription-manager repos --enable rhel-7-server-optional-rpms`  

Установить следующие пакеты `httpd mod_ssl mod_ldap`  
Так как у нас нет подписки на дополнительные репозитории я ставил из репозитриев CentOS 7, пакеты Centos собираются из исходников которые публикует RedHat поэтому cовместимость очень высокая.  
`cat /etc/yum.repos.d/centos7.repo`  
```
[centos7-repo]
name=Centos 7 package
baseurl=http://mirror.centos.org/centos-7/7/os/x86_64/
enabled=0
gpgcheck=0
```  
`yum --enablerepo=centos7-repo install httpd mod_ssl mod_ldap`  
`systemctl enable httpd`  

> Для безопастного подключения к ldaps потребуется сертфикат удостоверяющего центра, которым подписан сертификат который использует ldaps.  
> Если формат сертфикат будет `DER` тогду следует использовать `CA_DER`, если формат `PEM` тогда `CA_BASE64`.   
> Также AD может потребовать учетную запись, для запросов. Также при создания учетки в AD следуют задавать *User ID* и использовать его при авторизации.    
> Для авторизации в заголовки добавить строчку `Authorization: Basic YXBhY2hlX3Byb3h5OnBhc3N3b3Jk`, где `YXBhY2hlX3Byb3h5OnBhc3N3b3Jk` в кодировке base64 последовательность символов `apache_proxy:password`   
> `cn=apache-ldap-access` - это группа входя в которую, учетная запись будет иметь доступ. 


Конфиг положить здесь `/etc/http/conf.d/proxy-smf.conf`
```
LoadModule ssl_module modules/mod_ssl.so
LoadModule authnz_ldap_module modules/mod_authnz_ldap.so

#LDAPTrustedGlobalCert CA_BASE64 "/etc/httpd/CA.pem"
#LDAPTrustedGlobalCert CA_DER "/etc/httpd/CA.cer"

Listen 4438
<VirtualHost *:4438>
  #ServerName www.example.com
  
  SSLEngine on
  SSLCertificateFile  /opt/sfd/keys/smf-t01asl-n1.cert.pem
  SSLCertificateKeyFile /opt/sfd/keys/smf-t01asl-n1.key
    
  ProxyPass / http://10.70.39.16:8082/
  ProxyPassReverse / http://10.70.39.16:8082/
  
  <Location />
    AuthType Basic
    AuthName "Required auth"
    AuthBasicProvider ldap
    #AuthLDAPURL "ldaps://10.70.39.231:636/ou=rsa_atm_people,dc=gpb,dc=ru?uid?sub?(objectClass=*)"
    AuthLDAPURL "ldap://10.70.39.231:389/ou=rsa_atm_people,dc=gpb,dc=ru?uid?sub?(objectClass=*)"
    #AuthLDAPBindDN "apache_proxy@gpb.ru"
    #AuthLDAPBindPassword password
    Require ldap-group cn=apache-ldap-access,ou=rsa_atm_people,dc=gpb,dc=ru
  </Location>
</VirtualHost>
```  
`systemctl start httpd`  
